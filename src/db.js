const mysql = require('mysql');
let config = require('./config/config');
let adminSchema = require('./schema/admin.sql.schema');
let companySchema = require('./schema/company.sql.schema');
let seatsSchema = require('./schema/seats.sql.schema');
let couponSchema = require('./schema/coupon.sql.schema');

const connection = mysql.createConnection({
    host: config.host,
    user: config.user,
    password:config.password
});

connection.query("create database if not exists "+ config.database, function (err, results) {
    if (err) {
        console.log("ERROR:" + err);
    } else {
        console.log('DB created');
        createCompanyTable();
        createAdminTable();
        createSeatsTable();
        createCouponsTable();
    }
});

connection.query("use "+ config.database, function (err, results) {
    if (err) {
        console.log(err);
    } else {
        console.log("using DB");
    }
});

function createCompanyTable() {
    connection.query(companySchema.companyTable(), (err, result) => {
        if (err) {
            console.log(err);
        } else {
            console.log('Companynames Table Created');
        }
    });
}

function createAdminTable() {
    connection.query(adminSchema.adminTable(), (err, result) => {
        if (err) {
            console.log("AdminTable error"+err);
        } else {
            console.log('AdminTable Created');
        }
    });
}
function createSeatsTable() {
    connection.query(seatsSchema.seatsTable(), (err,result) => {
        if (err) {
            console.log(err);
        } else {
            console.log('SeatDetails Table Created');
        }
    });
}
function createCouponsTable() {
    connection.query(couponSchema.couponTable(), (err,result) => {
        if (err) {
            console.log(err);
        } else {
            console.log('CouponDetails Table Created');
        }
    });
}

module.exports = connection;
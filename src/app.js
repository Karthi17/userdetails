const express = require('express')
var session = require('express-session');
const bodyParser = require('body-parser');
const app = express()


var FrontEndPath = __dirname + '/../public/';
app.use(express.static(FrontEndPath)); //middle-ware


app.set('views', __dirname + '/views');
app.engine('html', require('ejs').renderFile);

app.use(session({ secret: 'ssshhh', saveUninitialized: true, resave: true}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

var sess;
app.get('/', function (req, res) {
  sess = req.session;
  if (sess.username) {
    res.redirect('/dashboard');
  }
  else {
    console.log(sess.username);
    res.redirect('/index.html');
  }
})

const admin = require('./routes/admin.route');

app.use('/admin', admin);


const dashBoard = require('./routes/dashboard.route');

app.use('/dashboard', dashBoard);
 
//end-point for logout session management
app.post('/logout', function (req, res) {
  req.session.destroy(function (err) {
    if (err) {
      console.log(err);
    }
    else {
      session = null;
      res.redirect('/');
    }
  });
});

//end-point to handle the invalid routes
app.get('/*', (req, res) => {
  res.redirect('/404redirect.html');
});


/**
 * Catching the JS error in the program
 */
app.use((err, req, res, next) => {
  console.log('app catch', err);

  // then let the app to crash,
  //because this programmer error, should be fixed immediately
  throw err;
});


app.listen(process.env.PORT||3000, (err, res) => {
  if (err) {
    console.log(err);
  } else {
    console.log(`Example app listening on 3000`);
  }
})
module.exports = sess;
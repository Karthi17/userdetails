module.exports = {
    adminTable: function () {
        return `CREATE TABLE IF NOT EXISTS AdminDetails(
                   id enum('1') NOT NULL,
                   AdminName VARCHAR(20) NOT NULL,
                   password VARCHAR(20) NOT NULL,
                   created_ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                   PRIMARY KEY(id))`;
    },
};

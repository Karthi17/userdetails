module.exports = {
    seatsTable : function () {
        return `CREATE TABLE IF NOT EXISTS SeatDetails(
                    id enum('1') NOT NULL,
                    TotalSeats INT NOT NULL,
                    OpenSeats INT NOT NULL,
                    PrivateSeats INT NOT NULL,
                    ConferenceHall INT NOT NULL,
                    OpenCount INT NOT NULL,
                    PrivateCount INT NOT NULL,
                    ConferenceCount INT NOT NULL,
                    created_ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                    PRIMARY KEY(id))`;
    }
}
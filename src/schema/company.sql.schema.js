module.exports = {
    companyTable : function () {
        return `CREATE TABLE IF NOT EXISTS Companynames(
                    id INT AUTO_INCREMENT NOT NULL,
                    FirstName varchar(20) NOT NULL,
                    LastName varchar(20) NOT NULL,
                    email VARCHAR(50) UNIQUE NOT NULL DEFAULT '0',
                    JoiningDate VARCHAR(10) NOT NULL,
                    LeavingDate VARCHAR(10) DEFAULT NULL,
                    CompanyName VARCHAR(20) NOT NULL DEFAULT '0',
                    TypeOfSeat VARCHAR(20) NOT NULL,
                    created_ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                    userID VARCHAR(100) UNIQUE,
                    password VARCHAR(20),
                    PRIMARY KEY(id))`;
    }
}
module.exports = {
    couponTable: function () {
        return `CREATE TABLE IF NOT EXISTS CouponDetails(
                    FirstName varchar(10) NOT NULL,
                    email VARCHAR(50) NOT NULL DEFAULT '0',
                    TypeOfCoupons VARCHAR(20) NOT NULL,
                    SavedDate TIMESTAMP DEFAULT CURRENT_TIMESTAMP)`
    },
};
const connection = require('../db');
const adminTable = require('../schema/admin.sql.schema');
var nodemailer = require("nodemailer");
var config = require('../config/config');

//function definition for Login end-point
let loginUser = function (params, callback) {
    const userDetails = {
        name: params.name,
        password: params.password
    };
    if (userDetails) {
        connection.query(`SELECT AdminName,password FROM AdminDetails WHERE AdminName='${userDetails.name}'`, (err, result) => {
            if (err) {
                callback(err);
            } else {
                if (result.length == 1) {
                    if (result[0].password == userDetails.password) {
                        let responseBody = {
                            "success": true,
                            "sendStatus": 202,
                            "msg": "Valid user"
                        };
                        callback(responseBody);
                    }
                    else {
                        let responseBody = {
                            "success": false,
                            "sendStatus": 403,
                            "msg": "No user found"
                        };
                        callback(responseBody);
                    }
                } else {
                    let responseBody = {
                        "success": false,
                        "sendStatus": 401,
                        "msg": "Not valid user"
                    };
                    callback(responseBody);
                }
            }
        })
    } else {
        let responseBody = {
            "success": false,
            "sendStatus": 403,
            "msg": "Not found in DB"
        };
        callback(responseBody);
    }
};
//using nodemailer to send mail to reset forgotpassword
var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: config.mailuser,
      pass: config.mailpassword
    }
  });

  let sendpassword = function (callback){
    var mailOptions = {  
        to: "karthitharani234@gmail.com", 
        subject:"Email from Gyanspace Admin",  
        text:"Reset your password",
        html: '<p>Click <a href="http://localhost:3000/reset.html">here</a> To reset your Password</p>'
    }  
    transporter.sendMail(mailOptions, function(err, result){
      if (err) {
        callback(err);
      } else {
        callback(result)
      }
    }); 
}
// function definition for updatepassword end-point
let updatepassword = function (params,callback){
    connection.query(`UPDATE admindetails SET password ='${params.password}' WHERE AdminName = '${'admin'}' `, (err, result, fields) => {
        if (err){
            callback(err);
        }
        else{
            callback(result);
        }
    } )
}

module.exports = {
    loginUser: loginUser,
    sendpassword:sendpassword,
    updatepassword:updatepassword
};
const connection = require('../db');
var nodemailer = require("nodemailer");
var config = require('../config/config');

//using nodemailer to send mail
var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: config.mailuser,
        pass: config.mailpassword
    }
});

//function definition for Listing Users end-point
let listingcompaniesFunction = function (callback) {
    connection.query(`SELECT * FROM companynames`, (err, result, fields) => {
        if (err) {
            callback(err);
        }
        else {
            callback(result);
        }
    })
}

let sendingMailFunction = function (params, callback) {
    var mailOptions = {
        to: params.Email,
        subject: "Email from Gyanspace Admin",
        html: '<div>Userid: ' + params.Email + '</div><div>Password: ' + params.password + '</div>'
    }
    transporter.sendMail(mailOptions, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(result)
        }
    });
}

//function definition for SeatDetails end-point
let seatdetailsFunction = function (callback) {
    connection.query(`SELECT * 
                      FROM seatdetails
                      WHERE id=1`, (err, result, fields) => {
            if (err) {
                callback(err);
            } else {
                callback(result);
            }
        })
};

let deleteFunction = function (params, callback) {
    const emailDetail = params.email
    const seattype = params.seattype
    if (emailDetail) {
        connection.query(`DELETE FROM companynames WHERE email='${emailDetail}'`, (err, result, fields) => {
            if (err) {
                callback(err);
            }
            else {
                connection.query(`SELECT * FROM seatdetails`, (err, result) => {
                    if (err) {
                        console.log(err)
                    } else {
                        oc = result[0].OpenCount
                        pc = result[0].PrivateCount
                        cc = result[0].ConferenceCount

                        //code to update the current seat count values
                        if (seattype == 'open') {
                            connection.query(`UPDATE seatdetails SET OpenCount=${--oc} where id=1`, (err, res) => {
                                if (err) console.log(err)
                                else console.log('openCount decremented Successfully')
                            })
                        }

                        else if (seattype == 'private') {
                            connection.query(`UPDATE seatdetails SET PrivateCount=${--pc} where id=1`, (err, res) => {
                                if (err) console.log(err)
                                else console.log('privateCount decremented Successfully')
                            })
                        }

                        else if (seattype == 'conference') {
                            connection.query(`UPDATE seatdetails SET ConferenceCount=${--cc} where id=1`, (err, res) => {
                                if (err) console.log(err)
                                else console.log('conferenceCount decremented Successfully')
                            })
                        }
                    }
                })
                callback(result);
            }
        })
    }
}


let editseatsFunction = function (params, callback) {
    const updatedSeatDetails = {
        totalseats: params.totalseats,
        openseats: params.openseats,
        privateseats: params.privateseats,
        conferenceseats: params.conferenceseats
    };
    if (updatedSeatDetails) {
        connection.query(`UPDATE seatdetails SET TotalSeats='${updatedSeatDetails.totalseats}',OpenSeats='${updatedSeatDetails.openseats}',PrivateSeats='${updatedSeatDetails.privateseats}',ConferenceHall='${updatedSeatDetails.conferenceseats}' where id=1`, (err, result, fields) => {
            if (err) {
                callback(err);
            } else {

                connection.query(`SELECT * FROM seatdetails WHERE id=1`, (err, result, fields) => {
                    if (err) {
                        console.log(err);
                    } else {
                        callback(result);
                    }
                })
            }
        })
    }
    else {
        let responseBody = {
            "success": false,
            "sendStatus": 403,
            "msg": "Not found in DB"
        };
        callback(responseBody);
    }
};
let listingCoupons = function (params, callback) {
    let emaild = params.email
    console.log(params.email)
    connection.query(`SELECT * FROM coupondetails WHERE email='${emaild}'`, (err, result) => {
        if (err) {
            callback(err);
        } else {
            callback(result);
            console.log(result)
        }
    })

}

let couponRemoveFunction = function (params, callback){
    const removeCoupon={
        email: params.email
    }
    if(removeCoupon){
        connection.query(`DELETE FROM coupondetails WHERE email='${removeCoupon.email}'`,(err, result) => {
            if (err) {
                callback(err);
            } else {
                callback(result);
            } 
        })
    }

};
//function definition for CouponDetails end-point
let couponFunction = function (params, callback) {
    const updatedcoupons = {
        FirstName: params.FirstName,
        email: params.email,
        TypeOfCoupons: params.TypeOfCoupons
    }
    if (updatedcoupons) {
        connection.query(`INSERT INTO coupondetails (FirstName,email,TypeOfCoupons) VALUES ('${updatedcoupons.FirstName}','${updatedcoupons.email}','${updatedcoupons.TypeOfCoupons}')`, (err, result, fields) => {
            if (err) {
                callback(err);
            } else {
                callback(result)
            }
        })
    }

};

//function definition for AddingUserDetails end-point
var QUERY = ''
let addingUserFunction = function (params, callback) {
    if (params.LeavingDate == '') {
        //value if leaving date is not choosed
        params.LeavingDate = null
    }

    QUERY = `INSERT INTO companynames SET ?`

    connection.query(QUERY, params, (err, result) => {

        if (err) {
            callback(err)
        } else {
            console.log('Success stored in DB')
            callback({
                'state': 'success',
                'FirstName': params.FirstName,
                'email': params.email,
                'CompanyName': params.CompanyName,
                'TypeOfSeats': params.TypeOfSeat,
                'userID': params.userID,
                'password': params.password,
                'StartDate': params.JoiningDate,
                'EndDate': params.LeavingDate
            })

            //variables used to store the current count of seats
            var oc = 0; var pc = 0; var cc = 0;

            //assigning seat counts from DB to corresponding variables
            connection.query(`SELECT * FROM seatdetails`, (err, result) => {
                if (err) {
                    console.log(err)
                } else {
                    oc = result[0].OpenCount
                    pc = result[0].PrivateCount
                    cc = result[0].ConferenceCount

                    //code to update the current seat count values
                    if (params.TypeOfSeat == 'open') {
                        connection.query(`UPDATE seatdetails SET OpenCount=${++oc} where id=1`, (err, res) => {
                            if (err) console.log(err)
                            else console.log('openCount Incremented Successfully')
                        })
                    }

                    else if (params.TypeOfSeat == 'private') {
                        connection.query(`UPDATE seatdetails SET PrivateCount=${++pc} where id=1`, (err, res) => {
                            if (err) console.log(err)
                            else console.log('privateCount Incremented Successfully')
                        })
                    }

                    else if (params.TypeOfSeat == 'conference') {
                        connection.query(`UPDATE seatdetails SET ConferenceCount=${++cc} where id=1`, (err, res) => {
                            if (err) console.log(err)
                            else console.log('conferenceCount Incremented Successfully')
                        })
                    }
                }
            })
        }
    })
}

module.exports = {
    seatdetailsFunction,
    addingUserFunction,
    editseatsFunction,
    listingcompaniesFunction,
    sendingMailFunction,
    deleteFunction,
    couponFunction,
    listingCoupons,
    couponRemoveFunction
};
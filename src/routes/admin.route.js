const express = require('express');
const router = express.Router();
const adminModel = require('../models/admin.model');

//end-point to verify the login credentials with DB
router.post('/login', (req, res) => {
    sess=req.session;
    sess.username=req.body.name;
    const userDetails = {
        name: req.body.name,
        password: req.body.password
    };
    
    adminModel.loginUser(userDetails,function(err,results){
        if(err){
            console.log(err);
            res.send(err);
        }else{
            return res.send(results);
        }
    })
});
//end-point to send mail
router.get('/sendpassword', (req, res) => {
    adminModel.sendpassword( function (result, err) {
        if (err) {
            console.log(err)
        } else if (result) {
            console.log('mail sent')
            res.send({
                'state': 'success'
            })
        }
    })  
})

router.post('/updatepassword', (req, res) => {
    const adminDetails = {
        password: req.body.confirmpassword
    };
    adminModel.updatepassword(adminDetails,function(err,results){
        if(err){
            console.log(err);
            res.send(err);
        }else{
            return res.send(results);
        }
    })
});

module.exports = router;
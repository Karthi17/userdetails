const express = require('express');
const router = express.Router();
const dashboardModel = require('../models/dashboard.model');

//end-point to retrive the seats details from DB
router.post('/SeatDetails', (req, res) => {
    sess = req.session;
    if (sess.username) {
        dashboardModel.seatdetailsFunction(function (err, result) {
            if (err) {
                res.send(err)
            } else {
                res.send(result)
            }
        })
    }
});
router.post('/couponDetails', (req, res) => {
    sess = req.session;
    if (sess.username) {
        const listCoupons = {
            email :req.body.emaild
        }
        console.log(req.body)
        dashboardModel.listingCoupons(listCoupons, function (err, result) {
            if (err) {
                res.send(err)
            } else {
                res.send(result)
                console.log(result);
            }
        })
    }
})
router.post('/coupons', (req, res) => {
    sess = req.session;
    if (sess.username) {
        const coupondetails = {
            FirstName: req.body.FirstName,
            email: req.body.email,
            TypeOfCoupons: req.body.TypeOfCoupons,
        };
        dashboardModel.couponFunction(coupondetails, function (result,err) {
            if (result) {
                res.send(result)
            } else {
                res.send(err)
            }
        })
    }
});

router.post('/couponDelete', (req, res) => {
    sess = req.session;
    if (sess.username) {
        const couponId = {
            email: req.body.emailId
        };
        dashboardModel.couponRemoveFunction(couponId, function (result,err) {
            if (result) {
                res.send(result)
            } else {
                res.send(err)
            }
        })
    }
});
//end-point to delete the row
router.post('/deleteRow', (req, res) => {
    sess = req.session;
    if (sess.username) {
        const details = {
            email: req.body.email,
            seattype: req.body.seattype
        }
        dashboardModel.deleteFunction(details, function (err, result) {
            if (err) {
                res.send(err)
            } else {
                console.log(success)
                res.send(result)
            }
        })
    }
});

//end-point to update the seats details into the DB
router.post('/EditSeats', (req, res) => {
    sess = req.session;
    if (sess.username) {
        const updatedSeatDetails = {
            totalseats: req.body.totalseats,
            openseats: req.body.openseats,
            privateseats: req.body.privateseats,
            conferenceseats: req.body.conferenceseats
        };
        dashboardModel.editseatsFunction(updatedSeatDetails, function (err, result) {
            if (err) {
                res.send(err)
            } else {
                return res.send(result)
            }
        })
    }
})
//end-point to store the form details into DB
router.post('/addUser', (req, res) => {
    sess = req.session;
    if (sess.username) {
        dashboardModel.addingUserFunction(req.body, function (err, result) {
            if (err) {
                res.send(err)
            } else {
                return res.send(result)
            }
        })
    }
})

//end-point to send mail
router.post('/sendmail', (req, res) => {
    sess = req.session;
    if (sess.username) {
        dashboardModel.sendingMailFunction(req.body, function (result, err) {
            if (err) {
                res.send(err)
            } else if (result) {
                console.log('mail sent')
                res.send({
                    'state': 'success'
                })
            }
        })
    }
})

router.post('/companyList', (req, res) => {
    sess = req.session;
    if (sess.username) {
        dashboardModel.listingcompaniesFunction(function (result, err) {
            if (err) {
                res.send(err)
            } else {
                res.send(result)
            }
        })
    }
});

module.exports = router;
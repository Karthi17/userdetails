$(document).ready(function () {
    if (!window.localStorage.getItem('isLoggedIn')) {
        if (window.location.pathname != '/') window.location.href = '/';
    }
    //validation for dates
    $('#EndDate').blur(() => {
        var StartDate = $('#StartDate').val()
        var EndDate = $('#EndDate').val()

        if (StartDate.length == 10 && EndDate.length == 10) {
            if (EndDate < StartDate) {
                alert('Enter correct Leaving Date')
                $('#EndDate').val('')
            }
        }
    })


    $("#save").click(function () {
        let FirstName = $("#modalname").html()
        FirstName = FirstName.replace("<b>Name : </b>", "")
        let email = $("#modalemail").html()
        email = email.replace("<b>E-mail : </b>", "")
        let TypeOfCoupons = $("input[name='couponRadioBtn']:checked").val()
        let coupondata = {
            FirstName,
            email,
            TypeOfCoupons,
        }
        //sending the form values to be stored in DB
        console.log('hi')
        console.log(coupondata);
        $.ajax({
            type: 'POST',
            url: 'dashboard/coupons',
            data: coupondata,
            dataType: 'json',
            encode: true
        })
            .done((res) => {
                alert('Coupon Details stored successfully in DB')
                location.reload();
            })
            .fail(function (error) {
                console.log(error);
            })

    })
    $('#formBtn').submit((e) => {
        e.preventDefault()
    })

    //request to the server to obtain the seat details
    $.ajax({
        type: 'POST',
        url: 'dashboard/SeatDetails',
        dataType: 'json',
        encode: true
    })
        .done((res) => {
            //code to disable type of seats radio button if availability is 0
            document.getElementById('openSeatBadge').innerHTML = `${res[0].OpenSeats - res[0].OpenCount}`
            if (`${res[0].OpenSeats - res[0].OpenCount}` == 0) {
                $("#defaultInline1").attr('disabled', true)
            } else {
                $("#defaultInline1").attr('disabled', false)
            }
            document.getElementById('privateSeatBadge').innerHTML = `${res[0].PrivateSeats - res[0].PrivateCount}`
            if (`${res[0].PrivateSeats - res[0].PrivateCount}` == 0) {
                $("#defaultInline2").attr('disabled', true)
            } else {
                $("#defaultInline2").attr('disabled', false)
            }
            document.getElementById('conferenceBadge').innerHTML = `${res[0].ConferenceHall - res[0].ConferenceCount}`
            if (`${res[0].ConferenceHall - res[0].ConferenceCount}` == 0) {
                $("#defaultInline3").attr('disabled', true)
            } else {
                $("#defaultInline3").attr('disabled', false)
            }
        })
        .fail((err) => {
            console.log(err)
        })

    //code to show only the corresponding card when a sideNav button is clicked
    $("#AddNewUser").hide();
    $("#ListingUser").show();
    $("#SeatManagement").hide();
    $("#Anav").click(() => {
        $("#AddNewUser").show()
        $("#ListingUser").hide()
        $("#SeatManagement").hide()
    })
    $("#Lnav").click(() => {
        $("#AddNewUser").hide()
        $("#ListingUser").show()
        $("#SeatManagement").hide()
    })
    $("#Snav").click(() => {
        $("#AddNewUser").hide()
        $("#ListingUser").hide()
        $("#SeatManagement").show()

        //request to the server to obtain the seat details from the DB table 
        $.ajax({
            type: 'POST',
            url: 'dashboard/SeatDetails',
            dataType: 'json',
            encode: true
        })
            .done(function (res) {

                //code to append the values in the corresponding line 
                $("#OverallSeats").html('Overall Total Seats : ' + res[0].TotalSeats)

                $("#OpenSeats").html('Total : ' + res[0].OpenSeats)
                $("#OpenUsed").html('Used : ' + res[0].OpenCount)
                $("#OpenAvailable").html('Available : ' + `${res[0].OpenSeats - res[0].OpenCount}`)
                var OpenPercent = res[0].OpenCount / res[0].OpenSeats * 100;
                var open = OpenPercent.toFixed(2);
                $("#OpenBar").css({ 'width': `${res[0].OpenCount / res[0].OpenSeats * 100}%` })
                $("#OpenBar").html(open + '%')


                $("#PrivateSeats").html('Total : ' + res[0].PrivateSeats)
                $("#PrivateUsed").html('Used : ' + res[0].PrivateCount)
                $("#PrivateAvailable").html('Available : ' + `${res[0].PrivateSeats - res[0].PrivateCount}`)
                var PrivatePercent = res[0].PrivateCount / res[0].PrivateSeats * 100;
                var private = PrivatePercent.toFixed(2);
                $("#PrivateBar").css({ 'width': `${res[0].PrivateCount / res[0].PrivateSeats * 100}%` })
                $("#PrivateBar").html(private + '%')


                $("#ConferenceHall").html('Total : ' + res[0].ConferenceHall)
                $("#ConferenceUsed").html('Used : ' + res[0].ConferenceCount)
                $("#ConferenceAvailable").html('Available : ' + `${res[0].ConferenceHall - res[0].ConferenceCount}`)
                var ConferencePercent = res[0].ConferenceCount / res[0].ConferenceHall * 100;
                var conference = ConferencePercent.toFixed(2);
                $("#ConferenceBar").css({ 'width': `${res[0].ConferenceCount / res[0].ConferenceHall * 100}%` })
                $("#ConferenceBar").html(conference + '%')

                //to update and display the total seats in each category of seats
                $("#totalseatsavailable").val(res[0].TotalSeats);
                $("#openseatsavailable").val(res[0].OpenSeats);
                $("#privateseatsavailable").val(res[0].PrivateSeats);
                $("#conferenceseatsavailable").val(res[0].ConferenceHall);

                // here we will handle errors and validation messages
            })
            .fail(function (error) {
                console.log(error);

            })
    });

    $("#navigate").click(() => {
        $('#side').show().delay(4000).fadeOut();
    })

});

//request to the server to fetch the user details
$.ajax({
    type: 'POST',
    url: 'dashboard/companyList',
    dataType: 'json',
    encode: true
})
    .done(function (res) {

        var tbl = document.getElementsByTagName('table')[0]
        for (var i in res) {
            var ID = i
            var tbody = document.getElementsByTagName('tbody')[0]
            var row = document.createElement('tr')
            var cell = document.createElement('td')
            cell.appendChild(document.createTextNode(++ID))
            row.appendChild(cell)
            var cell1 = document.createElement('td')
            cell1.appendChild(document.createTextNode(res[i].FirstName))
            row.appendChild(cell1)
            var cell2 = document.createElement('td')
            cell2.appendChild(document.createTextNode(res[i].email))
            row.appendChild(cell2)
            var cell3 = document.createElement('td')
            cell3.appendChild(document.createTextNode(res[i].CompanyName))
            row.appendChild(cell3)
            var cell4 = document.createElement('td')
            cell4.appendChild(document.createTextNode(res[i].TypeOfSeat))
            row.appendChild(cell4)
            var cell5 = document.createElement('td')
            var join = res[i].JoiningDate
            var Date1 = join.split('-').reverse().join('-')
            cell5.appendChild(document.createTextNode(Date1))
            row.appendChild(cell5)
            var cell6 = document.createElement('td')
            var leaveDate = ''
                if (res[i].LeavingDate == null)
                    leaveDate = '-'
                else
                    leaveDate = res[i].LeavingDate.split('-').reverse().join('-')
            cell6.appendChild(document.createTextNode(leaveDate))
            row.appendChild(cell6)
            var cell7 = document.createElement("BUTTON")
            row.appendChild(cell7)
            var t = document.createTextNode('Delete')
            cell7.appendChild(t);
            row.appendChild(cell7);
            tbody.appendChild(row)
            cell7.id = "btn";
            cell7.className = "btn btn-danger waves-effect btn-sm"
            cell7.addEventListener('click', function () {
                if (confirm("Are you sure want to delete the row?")) {
                    let email = $(this).prev().prev().prev().prev().prev().text()
                    let seattype = $(this).prev().prev().prev().text()
                    let emaildata = { email, seattype }
                    console.log(emaildata);
                    $.ajax({
                        type: 'POST',
                        url: 'dashboard/deleteRow',
                        data: emaildata,
                        dataType: 'json',
                        encode: true
                    })
                        .done(function () {
                            $(this).closest('tr').remove();
                            deleteCoupons(email);
                            location.reload(true);

                        })
                        .fail(function (error) {
                            console.log(error);
                        })
                }
            }, false);
            tbl.appendChild(tbody)
            var cell8 = document.createElement("BUTTON")
            row.appendChild(cell8)
            var s = document.createTextNode('Coupons')
            cell8.appendChild(s);
            row.appendChild(cell8);
            cell8.id = "btn";
            cell8.className = "btn btn-danger waves-effect btn-sm"
            cell8.addEventListener('click', function () {
                $('#basicModal').modal('show');
                name = $(this).prev().prev().prev().prev().prev().prev().prev().text()
                emaild = $(this).prev().prev().prev().prev().prev().prev().text()
                $("#modalname").html('<b>Name : </b>' + name)
                $("#modalemail").html('<b>E-mail : </b>' + emaild)
                let email = {
                    'emaild': emaild
                }
                $.ajax({
                    type: 'POST',
                    url: 'dashboard/couponDetails',
                    data: email,
                    dataType: 'json',
                    encode: true
                })
                    .done(function (res) {


                        if (res == '') {
                            $("#couponsincr").html("<b>Coupon Status:</b> Please select the type of Tokens")
                            $('input[name="couponRadioBtn"]').attr('disabled', false)
                            $('button[name="save"]').attr('disabled', false)
                        }
                        else if (res[0].TypeOfCoupons == 'daily') {
                            let currentDate = new Date();
                            currentDate.setHours(0, 0, 0, 0);
                            let savedDate = new Date(res[0].SavedDate);
                            savedDate.setHours(0, 0, 0, 0);

                            if (currentDate > savedDate) {
                                // deleteCoupons(res[0].email);
                                $("#couponsincr").html("<b>Coupon Status:</b> Please select the type of Tokens")
                                $('input[name="couponRadioBtn"]').attr('disabled', false)
                                $('button[name="save"]').attr('disabled', false)
                            }
                            else {
                                $("#couponsincr").html("<b>Coupon Status:</b> " + res[0].TypeOfCoupons + " coupon added!!")
                                $('input[name="couponRadioBtn"]').attr('disabled', 'disabled')
                                $('button[name="save"]').attr('disabled', true)
                            }
                        }
                        else if (res[0].TypeOfCoupons == 'month') {
                            let cD = new Date();
                            let currentDate = cD.getMonth() + 1;
                            cD.setHours(0, 0, 0, 0);
                            let sD = new Date(res[0].SavedDate);
                            let savedDate = sD.getMonth() + 1;
                            sD.setHours(0, 0, 0, 0);
                            if (currentDate > savedDate) {
                                // deleteCoupons(res[0].email);
                                $("#couponsincr").html("<b>Coupon Status:</b> Please select the type of Tokens")
                                $('input[name="couponRadioBtn"]').attr('disabled', false)
                                $('button[name="save"]').attr('disabled', false)
                            }
                            else {
                                $("#couponsincr").html("<b>Coupon Status: </b>" + res[0].TypeOfCoupons + "ly coupons added!!")
                                $('input[name="couponRadioBtn"]').attr('disabled', 'disabled')
                                $('button[name="save"]').attr('disabled', true)
                            }
                        }
                    })
                    .fail(function (error) {
                        console.log(error);
                    })
            })



        }

        tableCount = ++ID;

    })
    .fail(function (error) {
        console.log(error);
    })
function deleteCoupons(email) {
    let couponData={
        'emailId' : email
    }
    $.ajax({
        type: 'POST',
        url: 'dashboard/couponDelete',
        data: couponData,
        dataType: 'json',
        encode: true
    })
        .done(() => {
            alert("Your coupon has to be refilled");
        })
        .fail((err) => {
            console.log(error)
        })
}
//function definition for add function when the submit/add user button is clicked
function add() {

    //storing the input fields values in a variable
    let FirstName = $("#FirstName").val()
    let LastName = $("#LastName").val()
    let email = $("#Email").val()
    let JoiningDate = $("#StartDate").val()
    let LeavingDate = $("#EndDate").val()
    let CompanyName = $("#CompanyName").val()
    let TypeOfSeat = $("input[name='inlineDefaultRadiosExample']:checked").val()
    let userID = $("#Email").val()
    let password = $("#password").val();


    if (CompanyName == '') CompanyName = '-'
    //storing the form values in JSON format
    var data = {
        FirstName,
        LastName,
        email,
        JoiningDate,
        LeavingDate,
        CompanyName,
        TypeOfSeat,
        userID,
        password
    }

    //sending the form values to be stored in DB
    $.ajax({
        type: 'POST',
        url: 'dashboard/addUser',
        data: data,
        dataType: 'json',
        encode: true
    })
        .done((res) => {
            if (res.errno)
                alert(res.code + "\n" + res.sqlMessage)
            if (res.state == 'success') {
                $("body").css('opacity', 0.5)
                $("#loader").css('display', 'block')
                alert('User Details stored successfully in DB and mail has sent')
                mail()
                var tbl = document.getElementsByTagName('table')[0]
                var row = document.createElement('tr')
                var cell = document.createElement('td')
                cell.appendChild(document.createTextNode(tableCount))
                row.appendChild(cell)
                var cell1 = document.createElement('td')
                cell1.appendChild(document.createTextNode(res.FirstName))
                row.appendChild(cell1)
                var cell2 = document.createElement('td')
                cell2.appendChild(document.createTextNode(res.email))
                row.appendChild(cell2)
                var cell3 = document.createElement('td')
                cell3.appendChild(document.createTextNode(res.CompanyName))
                row.appendChild(cell3)
                var cell4 = document.createElement('td')
                cell4.appendChild(document.createTextNode(res.TypeOfSeats))
                row.appendChild(cell4)
                var cell5 = document.createElement('td')
                cell5.appendChild(document.createTextNode(res.JoiningDate))
                row.appendChild(cell5)
                var cell6 = document.createElement('td')
                var leaveDate = ''
                if (res[i].LeavingDate == null)
                    leaveDate = '-'
                else
                    leaveDate = res[i].LeavingDate
                cell6.appendChild(document.createTextNode(leaveDate))
                row.appendChild(cell6)
                tbl.appendChild(row)
            }
        })
        .fail((error) => {
            console.log(error)
        })
}

//function definition for generating password
function randomPassword(length) {
    var chars = "@#$%&+abcdefghijklmnopqrstuvwxyz1234567890";
    var password = "";
    for (var x = 0; x < length - 4; x++) {
        var i = Math.floor(Math.random() * chars.length);
        password += chars.charAt(i);
    }
    return password;
}
function generate() {

    if ($('#formBtn')[0].checkValidity()) {
        myform.row_password.value = randomPassword(myform.length.value);
        add()
    }

}

//function definition for sending mail
function mail() {
    //storing the input fields values in a variable
    var Email = $("#Email").val()
    var password = $("#password").val();

    //storing the form values in JSON format
    var data = {
        'Email': Email,
        'password': password
    }

    //request to send mail 
    $.ajax({
        type: 'POST',
        url: 'dashboard/sendmail',
        data: data,
        dataType: 'json',
        encode: true
    })
        .done((res) => {
            $("body").css('opacity', 1)
            $("#loader").css('display', 'none')
            console.log('Email send successfully to ' + Email);
            //refresh the page automatically once the form is submitted & the mail was sent
            location.reload()
        })
        .fail((error) => {
            $("body").css('opacity', 1)
            $("#loader").css('display', 'none')
            console.log("AjaxError in sendmail")
            console.log(error)
        })
}

//function definition for editing seats when the save button is clicked
function editseats() {
    var updatedSeats = {
        'totalseats': parseInt($('#openseatsavailable').val()) + parseInt($('#privateseatsavailable').val()) + parseInt($('#conferenceseatsavailable').val()),
        'openseats': $('#openseatsavailable').val(),
        'privateseats': $('#privateseatsavailable').val(),
        'conferenceseats': $('#conferenceseatsavailable').val(),

    }
    $.ajax({
        type: 'POST',
        url: 'dashboard/EditSeats',
        data: updatedSeats,
        dataType: 'json',
        encode: true
    })
        .done(function (res) {
            console.log("status: " + res.status);

            document.getElementById('openSeatBadge').innerHTML = res[0].OpenSeats
            document.getElementById('privateSeatBadge').innerHTML = res[0].PrivateSeats
            document.getElementById('conferenceBadge').innerHTML = res[0].ConferenceHall
            $("#totalseatsavailable").val(res[0].TotalSeats);
            $("#openseatsavailable").val(res[0].OpenSeats);
            $("#privateseatsavailable").val(res[0].PrivateSeats);
            $("#conferenceseatsavailable").val(res[0].ConferenceHall);

            $("#OverallSeats").html('Overall Total Seats : ' + res[0].TotalSeats)

            $("#OpenSeats").html('Total : ' + res[0].OpenSeats)
            $("#OpenUsed").html('Used : ' + res[0].OpenCount)
            $("#OpenAvailable").html(`Available : ${res[0].OpenSeats - res[0].OpenCount}`)
            var OpenPercent = res[0].OpenCount / res[0].OpenSeats * 100;
            $("#OpenBar").css({ 'width': `${res[0].OpenCount / res[0].OpenSeats * 100}% ` })
            $("#OpenBar").html(OpenPercent + '%')


            $("#PrivateSeats").html('Total : ' + res[0].PrivateSeats)
            $("#PrivateUsed").html('Used : ' + res[0].PrivateCount)
            $("#PrivateAvailable").html(`Available : ${res[0].PrivateSeats - res[0].PrivateCount}`)
            var PrivatePercent = res[0].PrivateCount / res[0].PrivateSeats * 100;
            $("#PrivateBar").css({ 'width': `${res[0].PrivateCount / res[0].PrivateSeats * 100}%` })
            $("#PrivateBar").html(PrivatePercent + '%')


            $("#ConferenceHall").html('Total : ' + res[0].ConferenceHall)
            $("#ConferenceUsed").html('Used :' + res[0].ConferenceCount)
            $("#ConferenceAvailable").html(`Available : ${res[0].ConferenceHall - res[0].ConferenceCount}`)
            var ConferencePercent = res[0].ConferenceCount / res[0].ConferenceHall * 100;
            $("#ConferenceBar").css({ 'width': `${res[0].ConferenceCount / res[0].ConferenceHall * 100}%` })
            $("#ConferenceBar").html(ConferencePercent + '%')
        })
        .fail(function (error) {
            console.log(error);
            console.log(error.responseText);
        })
}
//function to search data from listinguser table 
function searchFunction() {
    $("#search").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        $("tbody tr").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
}
//function definition for logout function
function logout() {
    window.localStorage.clear();

    $.ajax({
        type: 'POST',
        url: '/logout',
        encode: true
    }).done(function () {
        // log data to the console so we can see
        console.log("Logged Out Succesfully")
        window.location = '/'
    })
        // here we will handle errors and validation messages
        .fail(function () {
            window.location = '/'
        })

    // stop the form from submitting the normal way and refreshing the page
    event.preventDefault();
}
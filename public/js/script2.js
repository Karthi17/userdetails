$(document).ready(() => {
    if (window.localStorage.getItem('isLoggedIn')) {
        if (window.location.pathname == '/') window.location.href = '/dashboard.html';
    }
    $('#adminForm').submit((e) => {
        e.preventDefault()
    })
})

//function to be run when submit button is clicked in the login page
function login() {

    if ($('#adminForm')[0].checkValidity()) {
        //storing the login credentials in a JSON format
        var formData = {
            'name': $('#LoginName').val(),
            'password': $('#LoginPassword').val(),

        };

        //sending request to server along with login credentials
        $.ajax({
            type: 'POST',
            url: 'admin/login',
            data: formData,
            dataType: 'json',
            encode: true
        })
            .done(function (data) {
                window.localStorage.setItem('isLoggedIn', true)
                // log data to the console so we can see
                console.log("status: " + data.sendStatus);
                if (data.sendStatus == 202) {
                    window.location = '/dashboard.html'
                }
                if (data.sendStatus == 403) {
                    alert("password incorrect");
                    console.log('password incorrect')
                }
                if (data.sendStatus == 401) {
                    alert("invalid credentials")
                    console.log('invalid credentials')
                }
                if (data.sendStatus == 500) {
                    alert('Internal Server Error')
                    console.log('Internal Server Error')
                }


                // here we will handle errors and validation messages
            })
            .fail(function (error) {
                console.log(error);
            })
    }
}

//function definition for sending mail
function sendpassword() {
    $("body").css('opacity', 0.5)
    $("#loader").css('display', 'block')
    //request to send mail 
    $.ajax({
        type: 'GET',
        url: 'admin/sendpassword',
        encode: true
    })
        .done((res) => {
           
            alert('Check your mail to change password');
            console.log('Email send successfully to admin');
            $("body").css('opacity', 1)
            $("#loader").css('display', 'none')
            
            //refresh the page automatically once the form is submitted & the mail was sent
            location.reload()
        })
        .fail((error) => {
            
            console.log(error);
        })
}